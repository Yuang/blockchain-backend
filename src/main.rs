#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate rocket;

use rocket_contrib::serve::StaticFiles;
use rocket::response::NamedFile;
use std::path::PathBuf;
use std::path::Path;

#[get("/")]
fn index() -> Option<NamedFile> {
    NamedFile::open(Path::new("../frontend/dist1/index.html")).ok()
}

#[get("/1")]
fn indax() -> &'static str {
    "123"
}

#[get("/<file..>")]
fn files(file: PathBuf) -> Option<NamedFile> {
    let p = Path::new("../frontend/dist1/").join(file);
    NamedFile::open(p).ok()
}

#[get("/api/web/deviceinfo")]
fn web_for_deviceinfo() -> &'static str {

}

fn main() {
    let server = rocket::ignite()
        .mount("/", routes![index,indax])
        // .mount("/static",StaticFiles::from("../frontend/dist"))
        .mount("/static", routes![files])
        ;
    // .mount("/1/",routes![indax]);
    server.launch();
}